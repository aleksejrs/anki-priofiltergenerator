#!/bin/bash
WORKDIR=work
rm -f -- $WORKDIR/TAKEAWAY-*

echo SEA-аа-несроч
(
cat data/срочность-несрочное.ankisearch
echo -n " -("
cat data/срочность-срочное.ankisearch
echo ") "
)|egrep -v "^\s*(;|--)">$WORKDIR/SEA-аа-несроч

echo SEA-аа-сроч
(
#echo -n "-"
#cat срочность-несрочное|egrep -v "^\s*--"
cat data/срочность-срочное.ankisearch
)|egrep -v "^\s*(;|--)">$WORKDIR/SEA-аа-сроч

echo SEA-вв-неваж
(
cat data/важность-неважное-prefix.ankisearch
cat data/важность-неважное-topics.ankisearch
cat data/важность-неважное-suffix.ankisearch
echo -n " -("
cat data/важность-важное.ankisearch
echo ") "
)|egrep -v "^\s*(;|--)">$WORKDIR/SEA-вв-неваж

echo SEA-вв-неваж-nodeck
(
cat data/важность-неважное-prefix-nodeck.ankisearch
cat data/важность-неважное-topics.ankisearch
cat data/важность-неважное-suffix.ankisearch
)|egrep -v "^\s*(;|--)">$WORKDIR/SEA-вв-неваж-nodeck

echo SEA-вв-важ
(
#echo -n "-"
#cat SEA-вв-неваж-nodeck.ankisearch
cat data/важность-важное.ankisearch
)|egrep -v "^\s*(;|--)">$WORKDIR/SEA-вв-важ



echo TAKEAWAY-не_важ
(
cat data/prefix.ankisearch
echo -n " -"
cat data/важность-важное.ankisearch
)>$WORKDIR/TAKEAWAY-не_важ


echo TAKEAWAY-не_сроч
(
cat data/prefix.ankisearch
echo " (-added:3 -is:new) or (-added:1 is:new)"
echo -n " -"
cat data/срочность-срочное.ankisearch
)>$WORKDIR/TAKEAWAY-не_сроч


echo TAKEAWAY-неваж
(
cat data/prefix.ankisearch
echo -n " -"
cat data/важность-важное.ankisearch
cat data/важность-неважное-prefix-nodeck.ankisearch
#cat data/важность-неважное-prefix.ankisearch
cat data/важность-неважное-topics.ankisearch
cat data/важность-неважное-suffix.ankisearch
)>$WORKDIR/TAKEAWAY-неваж


echo TAKEAWAY-несроч
(
cat data/prefix.ankisearch
echo " (-added:10 -deck:*IR -is:new) or (-added:7 deck:*IR) or (-added:3 is:new)"
echo -n " -"
cat data/срочность-срочное.ankisearch
cat data/срочность-несрочное.ankisearch
)>$WORKDIR/TAKEAWAY-несроч

echo SEA for REPL

rm work/REPL.py

function for-repl-sea {
(
echo -n "mw.col.conf['savedFilters']['$1'.decode('utf8')] = "
echo -n '"""('
cat $WORKDIR/SEA-$1|egrep -v "^\s*(;|--)"|tr "\n\t" " "
echo -n ')"""'
echo ".decode('utf8')"
)>>work/REPL.py
echo>>work/REPL.py
echo>>work/REPL.py

}

for-repl-sea вв-важ
for-repl-sea вв-неваж
for-repl-sea аа-сроч
for-repl-sea аа-несроч

function for-repl-takeaway {
(
echo "did = mw.col.decks.id('~filt::prio::$1'.decode('utf8'))"
echo "deck = mw.col.decks.get(did)"
echo "assert deck['dyn']"
echo -n "deck['terms'][0][0] = '''"
cat $WORKDIR/TAKEAWAY-$1|egrep -v "^\s*(;|--)"|tr "\n\t" " "
echo -n "'''"
echo ".decode('utf8')"

)>>work/REPL.py
echo>>work/REPL.py

}

for-repl-takeaway неваж
for-repl-takeaway не_важ
for-repl-takeaway несроч
for-repl-takeaway не_сроч
echo Готово.

gvim work/REPL.py

